package android.controllers;

import android.HibernateDao.UserDao;
import android.entity.User;
import android.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Zbigniew on 2017-04-26.
 */
@Controller
public class UserController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserService userService;

    @RequestMapping("/")
    @ResponseBody
    public String welcome() {
        return "welcome";
    }

    @RequestMapping("/create")
    @ResponseBody
    public String create(String username, String password, String name, String email) {
        try {
            User newUser = new User(username, password, true, email, name);
            userService.createUser(newUser);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return ex.getMessage();
        }
        return "Uzytkownik zostal stworzony";
    }

    /**
     * GET /delete  --> Delete the user having the passed id.
     */
    @RequestMapping("/delete")
    @ResponseBody
    public String delete(int id) {
        boolean success = false;
        try {
            if (userService.deleteUser(id)) success = true;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "Error deleting the user:" + ex.getMessage();
        }
        System.out.println(success);
        return "User2 succesfully deleted!";
    }

    /**
     * GET /get-by-email  --> Return the id for the user having the passed
     * email.
     */
    @RequestMapping("/get-by-username")
    @ResponseBody
    public String getByEmail(String username) {
        String email;
        try {
            android.entity.User user = userService.getUserByUsername(username);
            email = user.getEmail();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return ex.getMessage();
        }
        return "User2: " + username + " has email: " + email;
    }

    /**
     * GET /update  --> Update the email and the name for the user in the
     * database having the passed id.
     */
    @RequestMapping("/update")
    @ResponseBody
    public String updateUser(int id, String email, String name) {
        try {
            android.entity.User user = userService.getUserById(id);
            user.setEmail(email);
            user.setName(name);
            userService.updateUser(user);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "Error updating the user:" + ex.toString();
        }
        return "User2 successfully updated";
    }
}
