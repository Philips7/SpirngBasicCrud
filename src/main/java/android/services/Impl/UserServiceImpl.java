package android.services.Impl;


import android.HibernateDao.UserDao;
import android.entity.User;
import android.services.UserService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Zbigniew on 2017-04-28.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public boolean createUser(User newUser) {
        boolean success = false;
        try {
            userDao.createUser(newUser);
            success = true;
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
        }
        return success;
    }

    @Override
    public User getUserByUsername(String username) {
        try {
            return userDao.getUserByUsername(username);
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

    @Override
    public User getUserById(int id) {
        try {
            return userDao.getUserById(id);
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

    @Override
    public boolean updateUser(User newUser) {
        boolean success = false;
        try {
            userDao.createUser(newUser);
            success = true;
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
        }
        return success;
    }

    @Override
    public boolean deleteUser(int id) {
        boolean success = false;
        try {
            userDao.deleteUser(id);
            success = true;
        } catch (HibernateException ex){
            System.out.println(ex.getMessage());
        }
        return success;
    }
}
