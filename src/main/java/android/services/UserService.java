package android.services;

import android.entity.User;

/**
 * Created by Zbigniew on 2017-04-28.
 */
public interface UserService {

    boolean createUser(User newUser);

    User getUserByUsername(String username);
    User getUserById(int id);

    boolean updateUser(User newUser);

    boolean deleteUser(int id);

}
