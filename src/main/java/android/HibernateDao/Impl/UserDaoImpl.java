package android.HibernateDao.Impl;

import android.HibernateDao.UserDao;
import android.entity.User;
import android.entity.UserRole;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Zbigniew on 2017-04-28.
 */
@Repository
@Transactional
public class UserDaoImpl implements UserDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public void createUser(User newUser) {
        Session session = sessionFactory.getCurrentSession();

        UserRole userRole1 = new UserRole(newUser.getUsername(), "ROLE_USER");
        UserRole userRole2 = new UserRole(newUser.getUsername(), "ROLE_ADMIN");
        List<UserRole> newRoles = new ArrayList<UserRole>();
        newRoles.add(userRole1);
        newRoles.add(userRole2);
        newUser.setUserRoles(newRoles);
        for (UserRole role : newRoles){
            role.setUser(newUser);
        }

        session.saveOrUpdate(newUser);
        session.flush();
    }

    @Override
    public User getUserByUsername(String username) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from User where username = :username ").setString("username", username);
        return (User)query.uniqueResult();
    }

    @Override
    public User getUserById(int id) {
        Session session = sessionFactory.getCurrentSession();
        User user = session.get(User.class, id);
        session.flush();
        return user;
    }

    @Override
    public void updateUser(User newUser) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(newUser);
        session.flush();
    }

    @Override
    public void deleteUser(int id) {
        Session session = sessionFactory.getCurrentSession();
        User user = getUserById(id);
        session.delete(user);
        session.flush();
    }
}
