package android.HibernateDao;

import android.entity.User;

/**
 * Created by Zbigniew on 2017-04-28.
 */

public interface UserDao {

    void createUser(User newUser);

    User getUserByUsername(String username);
    User getUserById(int id);

    void updateUser(User newUser);

    void deleteUser(int id);
}
