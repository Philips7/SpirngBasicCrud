# SpringBasicCrud

Basic CRUD operations on remote server using remote database

See how to deploy your first app on heroku servers and setup environment [https://devcenter.heroku.com/articles/getting-started-with-java#introduction](https://devcenter.heroku.com/articles/getting-started-with-java#introduction)

## Server side of application. Spring Boot, Hibernate, Maven


### Build and run

#### 1. Configurations

Set your own database connection details and driver in ```application.properties```

Change **Procfile** if needded:


For localhost:
```
web: java -Xmx350m -Dserver.port=8080 -jar target/Android-1.0-SNAPSHOT.jar
```
For heroku server:
```
web: java $JAVA_OPTS -Dserver.port=$PORT -jar target/Android-1.0-SNAPSHOT.jar
```
#### 2. Run on localhost:
```
mvn clean
```
```
mvn package
```
```
heroku local:start
```

#### 3. Deploy on heroku
```
git add .
```
```
git commit -am "first commit"
```
```
git push heroku master
```
```
heroku ps:scale web=1
```
```
heroku open
```
#### 4. CRUD 

```
localhost:8080/create?username=newUser&password=123456&name=newUser&email=newUser@gmail.com
```
```
localhost:8080/get-by-username?username=newUser
```
```
localhost:8080/update?id=6&email=newUser@gmail.com&name=newUser
```
```
localhost:8080/delete?id=1
```
